from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)

app.config.from_object('scorpion.settings')

db = SQLAlchemy(app)

from scorpion import routes, models
