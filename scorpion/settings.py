#!/usr/bin/env python
# -*- coding: utf-8 -*-

from os.path import dirname, realpath, join

VERSION = (0, 0, 1)
__author__ = "Andres Aguilar"
__date__ = "19 Jul 2017"
__version__ = ",".join([str(x) for x in VERSION])

""" settings.py
"""

APP_NAME = 'Scorpion'
BASE_DIR = dirname(realpath(__file__))

# Set debug mode (only in develop mode)
DEBUG = True
MINIFY = not DEBUG  # Only minify in production mode

# DataBase configuration
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + join(BASE_DIR, 'app.db')
SQLALCHEMY_MIGRATE_REPO = join(BASE_DIR, 'db_repository')
SQLALCHEMY_TRACK_MODIFICATIONS = False
