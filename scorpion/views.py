#!/usr/bin/env python
# -*- coding: utf-8 -*-

from flask import make_response
from flask import render_template

VERSION = (0, 0, 1)
__author__ = "Andres Aguilar"
__date__ = "19 Jul 2017"
__version__ = ",".join([str(x) for x in VERSION])

""" views.py

- Views definitions 
- Data formatter to render
"""


def index_view():
    """ Function: index_view
    Return the index page with all information and instructions
    to use the site
    """
    return render_template("welcome.jinja2")


def error_view():
    """ Function: error_view
    Return the 404 page
    """
    resp = make_response(render_template('404.html'), 404)
    resp.headers['X-ErrorStatusMessage'] = 'error 404'
    return resp


def search_view():
    """ Function: search_view
    Display form to search by libraries or by gene list
    """
    pass
