#!/usr/bin/env python
# -*- coding: utf-8 -*-

from __future__ import print_function

from htmlmin.main import minify

from . import app
from . import views

VERSION = (0, 0, 1)
__author__ = "Andres Aguilar"
__date__ = "19 Jul 2017"
__version__ = ",".join([str(x) for x in VERSION])

""" routes.py

Application main script
- Routes definitions
- Logger setup and configuration
- Request and error handlers

Like routes.py in Django
"""


@app.after_request
def response_minify(response):
    """ Function: response_minify
    minify html response to decrease site traffic
    """
    if app.config['MINIFY']:
        if response.content_type == u'text/html; charset=utf-8':
            response.set_data(minify(response.get_data(as_text=True)))
            return response
    return response


@app.route('/')
@app.route('/index')
def index():
    """ Function: index
    Handle request for the index view
    """
    return views.index_view()


@app.route('/search')
def search():
    """ Search view
    get and process the request form
    """
    pass


def search_by_gene_list():
    """ Function: search_by_gene_list
    Search by gene list (AT.., AT.., AT..) between multiple libraries
    """
    pass


def search_by_library():
    """ Function: search_by_library
    Search whole library
    """
    pass


@app.route('/results')
def get_results():
    """ Function: get_results
    Get search results and send it to the client
    """
    pass


@app.errorhandler(404)
def not_found(error):
    """ Function: not_found
    Handle 404 error

    :param error: Error information
    :return: 404 error view
    """
    app.logger.warning(error)
    return views.error_view()
