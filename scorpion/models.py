#!/usr/bin/env python
# -*- coding: utf-8 -*-

from . import db

from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.orm import relationship

VERSION = (0, 0, 1)
__author__ = "Andres Aguilar"
__date__ = "19 Jul 2017"
__version__ = ",".join([str(x) for x in VERSION])

""" models.py

- Models definition
- Data structures
- DataBase connection
"""

# TODO: Check migration schema (https://sqlalchemy-migrate.readthedocs.io/en/v0.7.2/index.html#)
# TODO: Write models for libraries, genes and replicates


class LibraryModel(db.Model):
    """ Class: LibraryModel
    Model describing all attributes for a library
    """
    id = Column(Integer, primary_key=True)
    name = Column(String(128), index=True, unique=True)
    origin = Column(String(128))
    lib_type = Column(String(64))
    id_jbrowse = Column(String(50))
    development_state = Column(String(100))
    replicate_id = Column(Integer, ForeignKey('ReplicateModel.replicate_id'))

    # Each library have 1..n replicates
    replicates = relationship('ReplicateModel')

    def __repr__(self):
        return '<Library: {0}>'.format(self.name)


class ReplicateModel(db.Model):
    """ Class: ReplicateModel
    """
    replicate_id = Column(Integer, primary_key=True)
    rep_number = Column(String(50), index=True, unique=True)
    counts_table = Column(String(200))

    def __repr__(self):
        return '<Replicate: {0}>'.format(self.rep_number)


class GeneModel(db.Model):
    """ Class: GeneModel
    """
    gene_id = Column(Integer, primary_key=True)
    identifier = Column(String(10), unique=True, index=True)
    symbol = Column(String(10), unique=True, index=True)
    name = Column(String(100))
    start_coord = Column(Integer)
    end_coord = Column(Integer)