#!/usr/bin/python
# -*- coding: utf-8 -*-

from scorpion import app
from scorpion import settings


if __name__ == "__main__":
    app.run(debug=settings.DEBUG)
